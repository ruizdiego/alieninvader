﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour
{
	// Tiempo que el objeto permanecerá visible
	public float visibleTime = 1f;

	// Tiempo que el objeto permanecerá invisible
	public float invisibleTime = 1f;

	// Acumulador de tiempo
	float accumTime;

	// Referencia al renderer del mesh
	UnityEngine.UI.Text txtRef;

	// Unity Start Method
	void Start()
	{
		txtRef = GetComponent<UnityEngine.UI.Text>();

		Debug.AssertFormat(txtRef != null, "El objeto debe poseer un componente UnityEngine.UI.Text");
	}
	
	// Unity Update Method
	void Update()
	{
		accumTime += Time.deltaTime;

		if (txtRef.enabled && accumTime > visibleTime)
		{
			txtRef.enabled = false;
			accumTime = 0f;
		}
		else if (!txtRef.enabled && accumTime > invisibleTime)
		{
			txtRef.enabled = true;
			accumTime = 0f;
		}
	}
}
