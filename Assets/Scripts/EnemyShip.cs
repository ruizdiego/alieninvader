﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour
{
    public GameObject[] modelArr;

    AudioSource audSrc;
    SphereCollider sphColl;

    bool destroyed;

    void Start()
    {
        audSrc = GetComponent<AudioSource>();
        sphColl = GetComponent<SphereCollider>();
        destroyed = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (string.Compare(other.gameObject.tag, "Bullet") == 0)
        {
            audSrc.Play();

            Destroy(other.gameObject);

            modelArr[0].SetActive(false);
            modelArr[1].SetActive(false);

            sphColl.enabled = false;

            destroyed = true;

            // Invocar la función DestroyShield dentro de 1 segundo
            // Esto evita destruir inmediatamente el objeto cuando
            // aún existe un sonido ejecutandose
            Invoke("DestroyShip", 1f);

            // Notifación al gestor de puntaje que un enemigo ha sido destruido
            ScoreManager.NotifyEnemyKill();
        }
    }

    void DestroyShip()
    {
        Destroy(this.gameObject);
    }

    public void SetActiveModel(bool showModelOne)
    {
        if (!destroyed)
        {
            modelArr[0].SetActive(showModelOne);
            modelArr[1].SetActive(!showModelOne);
        }
    }
}
