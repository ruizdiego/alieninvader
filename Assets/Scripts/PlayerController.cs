﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float speed = 1f;

    public AudioClip playerKilled;

    int moveDirection;

    AudioSource audSrc;

    SphereCollider sphColl;

    MeshRenderer meshRnd;

    bool destroyed;

	// Use this for initialization
	void Start()
    {
        audSrc = GetComponent<AudioSource>();
        sphColl = GetComponent<SphereCollider>();
        meshRnd = GetComponent<MeshRenderer>();
        destroyed = false;
    
    }

	// Update is called once per frame
	void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveDirection = -1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            moveDirection = 1;
        }
        else
        {
            moveDirection = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }

        transform.position += new Vector3(moveDirection * speed * Time.deltaTime, 0f, 0f);
	}

    public void Shoot()
    {
        if (!destroyed)
        {
            GameObject bullet = Instantiate(bulletPrefab);
            bullet.transform.position = transform.position;
            bullet.GetComponent<Bullet>().Fire();

            audSrc.Play();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (string.Compare(other.gameObject.tag, "EnemyBullet") == 0)
        {
            audSrc.clip = playerKilled;
            audSrc.Play();

            Destroy(other.gameObject);

            meshRnd.enabled = false;
            sphColl.enabled = false;

            destroyed = true;

            // Invocar la función DestroyShield dentro de 1 segundo
            // Esto evita destruir inmediatamente el objeto cuando
            // aún existe un sonido ejecutandose
            Invoke("DestroyShip", 1f);


        }
    }

    void DestroyShip()
    {
        Destroy(this.gameObject);
    }
}
