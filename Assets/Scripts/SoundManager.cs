﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    static SoundManager instance;

    public AudioClip shieldImpact;
    AudioSource audSrc;

	// Use this for initialization
	void Start()
    {
        instance = this;
        audSrc = GetComponent<AudioSource>();
	}
	
    public static void ShieldImpact()
    {
        instance.audSrc.clip = instance.shieldImpact;
        instance.audSrc.Play();
    }
}
