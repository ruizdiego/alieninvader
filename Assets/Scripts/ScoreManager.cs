﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public UnityEngine.UI.Text scoreValueUIText;
    public UnityEngine.UI.Text hiscoreValueUIText;

    public int pointsPerKill = 10;

    int scoreValue;
    int hiScoreValue;

    static ScoreManager instance;

	// Unity Start Method
	void Start()
    {
        instance = this;

        hiScoreValue = PlayerPrefs.GetInt("hiscore", 0);
        instance.hiscoreValueUIText.text = string.Format("{0:D4}", instance.hiScoreValue);
	}

    void OnDestroy()
    {
        PlayerPrefs.SetInt("hiscore", hiScoreValue);
    }

    public static void NotifyEnemyKill()
    {
        instance.scoreValue += instance.pointsPerKill;
        instance.scoreValueUIText.text = string.Format("{0:D4}", instance.scoreValue);

        if (instance.scoreValue > instance.hiScoreValue)
        {
            instance.hiScoreValue = instance.scoreValue;
            instance.hiscoreValueUIText.text = string.Format("{0:D4}", instance.hiScoreValue);
        }
    }
}
