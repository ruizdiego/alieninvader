﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    AudioSource audSrc;
    BoxCollider boxColl;
    MeshRenderer meshRnd;

    void Start()
    {
        audSrc = GetComponent<AudioSource>();
        boxColl = GetComponent<BoxCollider>();
        meshRnd = GetComponent<MeshRenderer>();
    }

    void OnTriggerEnter(Collider other)
    {
        audSrc.Play();

        Destroy(other.gameObject);

        boxColl.enabled = false;
        meshRnd.enabled = false;

        // Invocar la función DestroyShield dentro de 1 segundo
        // Esto evita destruir inmediatamente el objeto cuando
        // aún existe un sonido ejecutandose
        Invoke("DestroyShield", 1f);
    }

    void DestroyShield()
    {
        Destroy(this.gameObject);
    }
}
