﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    float speed = 10f;

    void Update()
    {
        transform.position -= Vector3.up * speed * Time.deltaTime;

        if (transform.position.y < -7f)
        {
            Destroy(this.gameObject);
        }
    }

    public void Fire()
    {
    }
}
