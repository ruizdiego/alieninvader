﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarManager : MonoBehaviour
{
	const float MinPosX = -200f;
	const float MaxPosX = 200f;
	const float MinPosZ = 0f;
	const float MaxPosZ = 600f;
	const float MinStarScale = 0.2f;
	const float MaxStarScale = 3f;
	const float MinSpeed = 10f;
	const float MaxSpeed = 100f;

	// Referencia a un prefab de estrella
	public GameObject starPrefab;

	// Array de estrellas que se creará dinámicamente
	GameObject[] stars;

	// La velocidad de cada estrella (no será única para generar mayor diversidad)
	float[] speeds;

	// Cantidad de estrellas que crearemos
	int starNumToCreate = 100;

	// Posición por defecto de las estrellas
	float defaultZPos = 250f;

	// Velocidad de desplazamiento de las estrellas
	float scrollSpeed = 20f;

	// Unity Start Method
	void Start()
	{
		stars = new GameObject[starNumToCreate];
		speeds = new float[starNumToCreate];

		for (int i=0; i<starNumToCreate; i++)
		{
			// Creo que el objeto a partir de la referencia del prefab
			GameObject go = Instantiate(starPrefab);

			// Fijo su posición
			go.transform.position = new Vector3(Random.Range(MinPosX, MaxPosX), Random.Range(MinPosZ, MaxPosZ), defaultZPos);

			// Fijo su escala (la elijo de manera random entre dos valores)
			float scale = Random.Range(MinStarScale, MaxStarScale);
			go.transform.localScale = Vector3.one * scale;

			// Al objeto creado, le cambio su padre (en este caso
			// no es algo obligario sino sólo de prolijidad)
			go.transform.parent = transform;

			// Otro paso opcional: cambio su nombre para que sea exactamente el
			// mismo que el del prefab
			go.name = starPrefab.name;

			// Fijo la velocidad de la estrella de manera random entre dos valores
			speeds[i] = Random.Range(MinSpeed, MaxSpeed);

			// Asigno la referencia del objeto creado al elemento correspondiente del array
			stars[i] = go;
		}
	}
	
	// Unity Update Method
	void Update()
	{
		for (int i=0; i<starNumToCreate; i++)
		{
			// Muevo la estrella en el eje Y
			stars[i].transform.position = new Vector3(stars[i].transform.position.x, stars[i].transform.position.y - scrollSpeed * Time.deltaTime, stars[i].transform.position.z);

			// Roto la estrella
			stars[i].transform.RotateAround(stars[i].transform.position, new Vector3(0.7f, 0.7f, 0.7f), speeds[i] * Time.deltaTime);

			// Cuando la estrella alcanza la posición inferior de la pantalla, la vuelvo a colocar encima
			if (stars[i].transform.position.y < 0f)
				stars[i].transform.position = new Vector3(stars[i].transform.position.x, 600f, stars[i].transform.position.z);
		}
	}
}
