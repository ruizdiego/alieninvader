﻿// Mono Framework
using System.Collections.Generic;

// Unity Framework
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    // Array de referencias de prefabs de enemigos 
    public GameObject[] enemyTypePrefab;

    // Prefab de un disparo enemigo
    public GameObject enemyBulletPrefab;

    // Cantidad de enemigos a instanciar por línea
    public int enemyPerRow = 10;

    // Separación entre líneas de enemigos
    public float sepBetweenEnemyLines = 2f;

    // Posición de la primer línea de enemigos
    public float yInitialPos = 12f;

    // Posición por defecto en Z
    // Esta posición debe coincidir con el Z de los proyectiles para que
    // pueda existir un impacto
    public float zDefaultPos = 2f;

    // Sonido de avance (var 1)
    public AudioClip moveSnd1;

    // Sonido de avance (var 2)
    public AudioClip moveSnd2;

    // Acumulador de tiempo utilizado para el movimiento
    // de las naves
    float accumTime;

    // Velocidad de avance de las naves
    // Cada vez que el tiempo acumulador alcance este
    // número, las naves se moveran un paso
    float timeMovementStep = 1f;

    // Listado de enemigos instanciados
    List<GameObject>[] lstEnemy;

    // Dirección de movimiento (1: derecha, -1: izquierda)
    float movDir = 1f;

    // Posición máxima X
    float maxPosX;

    // Flag utilizado para saber que variación de sonido disparar
    bool moveSoundTicTac;

    // Referencia al componente AudioSource
    AudioSource audSrc;

    float movDeltaY = 0f;

    // Acumulador de tiempo utilizado para espaciar
    // los disparos enemigos
    float accumTimeShooting;

    // Cuando accumTimeShooting alcance este valor, se
    // realizará un disparo
    float shootStep = 1f;

	// Use this for initialization
	void Start()
    {
        Debug.AssertFormat(enemyTypePrefab != null, "enemyTypePrefab no puede ser nulo");

        audSrc = GetComponent<AudioSource>();

        lstEnemy = new List<GameObject>[enemyTypePrefab.Length];

        Camera cam = Camera.main;
        Vector3 minPos = cam.ScreenToWorldPoint(new Vector3(10f, cam.pixelHeight * 0.8f, 32f));
        Vector3 maxPos = cam.ScreenToWorldPoint(new Vector3(cam.pixelWidth - 10f, cam.pixelHeight * 0.8f, 32f));
        float sepX = (maxPos.x - minPos.x) / (enemyPerRow + 1);

        for (int i = 0; i < enemyTypePrefab.Length; i++)
        {
            lstEnemy[i] = new List<GameObject>();

            for (int j = 0; j < enemyPerRow; j++)
            {
                GameObject enemy = Instantiate(enemyTypePrefab[i]);
                enemy.name = string.Format("enemy-line-{0}-{1}", i, j);
                enemy.transform.position = new Vector3(minPos.x + sepX * j, yInitialPos + sepBetweenEnemyLines * i, zDefaultPos);
                enemy.transform.parent = transform;

                lstEnemy[i].Add(enemy);
            }
        }

        accumTime = 0f;

	}
	
	// Update is called once per frame
	void Update()
    {
        accumTime += Time.deltaTime;

        // 1. Lógica de movimiento de naves enemigas
        if (accumTime > timeMovementStep)
        {
            MoveEnemies();
            accumTime = 0f;
        }

        // 2. Lógica de disparo de naves enemigas
        accumTimeShooting += Time.deltaTime;

        if (accumTimeShooting >= shootStep)
        {
            ShootToPlayer();
            accumTimeShooting = 0f;
        }
	}

    void MoveEnemies()
    {
        float minPosX = float.MaxValue;
        float maxPosX = float.MinValue;

        for (int i = 0; i < lstEnemy.Length; i++)
        {
            for (int j = 0; j < lstEnemy[i].Count; j++)
            {
                GameObject enemyGO = lstEnemy[i][j];

                if (enemyGO != null)
                {
                    enemyGO.transform.position = new Vector3(enemyGO.transform.position.x + movDir * 0.5f, enemyGO.transform.position.y - movDeltaY, enemyGO.transform.position.z);

                    if (enemyGO.transform.position.x < minPosX)
                        minPosX = enemyGO.transform.position.x;

                    if (enemyGO.transform.position.x > maxPosX)
                        maxPosX = enemyGO.transform.position.x;

                    enemyGO.GetComponent<EnemyShip>().SetActiveModel(moveSoundTicTac);
                }
            }
        }

        if (movDeltaY > 0f)
            movDeltaY = 0f;

        if (movDir == 1f && maxPosX > 10f)
        {
            movDir = -1f;
            movDeltaY = 1f;
            timeMovementStep *= 0.9f;
        }
        else if (movDir == -1f && minPosX < -10f)
        {
            movDir = 1f;
            movDeltaY = 1f;
            timeMovementStep *= 0.9f;
        }

        if (moveSoundTicTac)
        {
            audSrc.clip = moveSnd1;
        }
        else
        {
            audSrc.clip = moveSnd2;
        }

        audSrc.Play();
        moveSoundTicTac = !moveSoundTicTac;
    }

    void ShootToPlayer()
    {
        // 1. Elijo una columna de enemigos donde aún queden enemigos vivos
        int col = Random.Range(0, enemyPerRow);

        Vector3 shootPos = Vector3.zero;

        // 2. Selecciono la nave que se encuentre en la fila inferior de dicha columna
        // (de otro modo las naves enemigas podrían matarse entre ellas o al menos
        // quedaría raro que un disparo enemigo atraviese sus propias naves sin matarlas)
        //for (int i = lstEnemy.Length - 1; i >= 0; i--)
        for (int i=0; i<lstEnemy.Length; i++)
        {
            if (lstEnemy[i][col] != null)
            {
                shootPos = lstEnemy[i][col].transform.position;
                break;
            }
        }

        GameObject enemyBulletGO = Instantiate(enemyBulletPrefab);
        enemyBulletGO.transform.position = shootPos;
        //EnemyBullet eb = enemyBulletGO.GetComponent<EnemyBullet>();
    }
}
